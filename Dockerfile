# Dockerfile to build and run the module on the cloud

# Stage 1 python build
FROM registry.gitlab.com/vlabsdev/core-applications/jigyasa-sites/jigyasa-lab-templates/template-combined-cdn-links/main:latest AS builder
ARG PROJECT_NAME
ARG NODAL_NAME
ARG WGET_TOKEN
ARG CDN_LINK
COPY ./manual /app/manual/
COPY details.json /app
RUN ls -ltr
# integration by python
RUN python3 script.py
RUN rm *.py
RUN rm *.txt

# Dockerfile to build and run the module on the cloud
# Stage 2 s3 push
FROM d3fk/s3cmd AS s3-bucket
ARG S3_AKEY
ARG S3_SKEY
ARG PROJECT_NAME
ARG NODAL_NAME
ARG S3_LAB_MANUAL
ARG S3_ACTIVITY
ARG S3_ACL
ARG S3_HOST
ARG CDN_LINK

# Pushing BUILD CONTEXT to CDN
RUN mkdir -p /home/$NODAL_NAME/$PROJECT_NAME
WORKDIR /home
COPY --from=builder /app /home/$NODAL_NAME/$PROJECT_NAME
RUN ls -ltr 
RUN s3cmd put --recursive /home/ $S3_LAB_MANUAL/ --host-bucket=bucket --host=$S3_HOST --no-check-certificate --access_key=$S3_AKEY --secret_key=$S3_SKEY

# Pushing ACTIVITY to CDN
RUN mkdir -p activity/$NODAL_NAME
COPY ./activity /home/activity/$NODAL_NAME
RUN mv /home/activity/$NODAL_NAME/*.mp4 /home/activity/$NODAL_NAME/$PROJECT_NAME.mp4 
RUN ls -ltr /home/activity/$NODAL_NAME/
RUN s3cmd put --recursive /home/activity/ $S3_ACTIVITY/ --host-bucket=bucket --host=$S3_HOST --no-check-certificate --access_key=$S3_AKEY --secret_key=$S3_SKEY
RUN s3cmd setacl $S3_ACL  $S3_ACTIVITY/$NODAL_NAME/$PROJECT_NAME.mp4 --host-bucket=bucket --host=$S3_HOST --no-check-certificate --access_key=$S3_AKEY --secret_key=$S3_SKEY 

